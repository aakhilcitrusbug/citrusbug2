


function openNav2() {
    $("#mySidenav2").addClass("width-menu");
    $("#cd-shadow-layer").css("display", "flex");
    $("body").css("position", "relative");
    $("body").css("overflow", "hidden");
    $("body").css("height", "100vh");
    // $(".position-fixed-overlay").addClass("position-show");
    $(".closebtn2").css("position", "fixed");
   
  
  }
  
  function closeNav2() {
    $("#mySidenav2").removeClass("width-menu");
    $("#cd-shadow-layer").css("display", "none");
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
  
  }
  

// wow
$(document).ready(function(){

new WOW(
    {  
        mobile:  false,
    }
).init();
    
   

// nav slide




/// smooth scroll

$('a.smoth-scroll').on("click", function (e) {
    var anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top - 70
    }, 1500);
    e.preventDefault();
});

// header sticky

var headertopoption = $(window);
var headTop = $('.navbar-dark');

headertopoption.on('scroll', function () {
    if (headertopoption.scrollTop() > 0) {
        headTop.addClass('fixed-top');
    } else {
        headTop.removeClass('fixed-top');
    }
});

// menu click

$(".nav-link").click(function(){
    $(".nav-link").removeClass("active");
    $(this).addClass("active");
});


$(".nav-link").click(function(){
    $(".navbar-collapse").removeClass("show");

    $("#mySidenav2").removeClass("width-menu");
    $("#cd-shadow-layer").css("display", "none");
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
});


// home

$(".url").click(function(){
    $(".url").removeClass("active");
    $(this).addClass("active");
});




// timeline js

$(".StepProgress li a").click(function(){
    $(".StepProgress li").removeClass("active fadeInUp");
    $(this).parent().addClass("active fadeInUp");
});


// readmore


$("#toggle-read").click(function() {
    var elem = $("#toggle-read").text();
    if (elem == "Read More...") {
      $("#toggle-read").text("Read Less");
      $("#text_hide_show").show();
    } else {
      $("#toggle-read").text("Read More...");
      $("#text_hide_show").hide();
    }
  });



// owl carousel our section

// home page banner

var owl = $('.owl-carousel-home').owlCarousel({
    loop:true,
    margin:10,
    smartSpeed:2000,
    autoplay:true,
    autoplayHoverPause:true,
    dots: false,
    nav: false,
    responsiveClass:true,
    URLhashListener:true,
    startPosition: 'URLHash',
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            loop:true,
        }
    }
});


owl.on('changed.owl.carousel',function(property){
    var current = property.item.index;
    // var src = $(property.target).find(".owl-item").eq(current).find("img").attr('src');
    var src = $(property.target).find(".owl-item").eq(current).find('div').attr('class');
    console.log('Image current is ' + src);
    if((src)=="item zero"){
        $(".url.active").removeClass("active");
        $(".url.zero").addClass("active");
    };

    if((src)=="item one"){
        $(".url.active").removeClass("active");
        $(".url.one").addClass("active");
    };

    if((src)=="item one"){
        $(".url.active").removeClass("active");
        $(".url.one").addClass("active");
    };

    if((src)=="item two"){
        $(".url.active").removeClass("active");
        $(".url.two").addClass("active");
    };

    if((src)=="item three"){
        $(".url.active").removeClass("active");
        $(".url.three").addClass("active");
    };

    if((src)=="item four"){
        $(".url.active").removeClass("active");
        $(".url.four").addClass("active");
    };

    if((src)=="item five"){
        $(".url.active").removeClass("active");
        $(".url.five").addClass("active");
    };

});



// our client

$('.owl-carousel-our-client').owlCarousel({
    loop:true,
    margin:10,
    smartSpeed:2000,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icon/arrow-left-client.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icon/arrow-right-client.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },

        1000:{
            items:1,
            nav:true,
            dots: false
        },
     
    }
});

// logo-client


$('.owl-carousel-logo-client').owlCarousel({
    loop:true,
    margin: 0,
    smartSpeed:2000,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icon/arrow-left-client.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icon/arrow-left-client.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{

        320:{
            items:2,
            nav:false,
            stagePadding: 0,
           
            
        },
        375:{
            items:3,
            nav:false,
            stagePadding: 0,
            margin: -80,
        },
        600:{
            items:5,
            nav:false
        },

        1000:{
            items:8,
            nav:false,
            dots: false
        },
     
    }
});

// our service

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    smartSpeed:2000,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icon/arrow-left.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icon/arrow-right.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },

        1000:{
            items:2,
            nav:true
        },
        1025:{
            items:3,
            nav:true,
            loop:true
        }
    }
});





 });